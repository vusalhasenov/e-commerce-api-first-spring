package az.lead.ecommerceapifirstspring.service.impl;

import az.lead.ecommerceapifirstspring.dto.ProductDto;
import az.lead.ecommerceapifirstspring.entity.Product;
import az.lead.ecommerceapifirstspring.entity.ProductType;
import az.lead.ecommerceapifirstspring.entity.Unit;
import az.lead.ecommerceapifirstspring.enums.ResultCode;
import az.lead.ecommerceapifirstspring.enums.Status;
import az.lead.ecommerceapifirstspring.exception.AlreadyExistException;
import az.lead.ecommerceapifirstspring.exception.DataNotFoundException;
import az.lead.ecommerceapifirstspring.mapper.ProductMapper;
import az.lead.ecommerceapifirstspring.mapper.ProductStructMapper;
import az.lead.ecommerceapifirstspring.repository.ProductRepo;
import az.lead.ecommerceapifirstspring.repository.ProductTypeRepo;
import az.lead.ecommerceapifirstspring.repository.UnitRepo;
import az.lead.ecommerceapifirstspring.request.ProductReq;
import az.lead.ecommerceapifirstspring.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepo productRepo;
    private final ProductTypeRepo productTypeRepo;
    private final UnitRepo unitRepo;
    private final ProductStructMapper productStructMapper;


    @Override
    public List<ProductDto> findAll() {
        return productRepo.findAll().stream()
                .map(p -> productStructMapper.mapToDto(p))
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long id) {
        return productRepo.findById(id).map(product -> productStructMapper.mapToDto(product))
                .orElseThrow(() -> DataNotFoundException.productNotFound());
        /*if (!productRepo.findById(id).equals(null))
        return productRepo.findById(id).map(product -> mapToDto(product));
        else throw new  DataNotFoundException(ResultCode.PRODUCT_NOT_FOUND);*/
    }

    @Override
    public ProductDto save(ProductReq productReq) {
        //map
        ProductType productType = productTypeRepo.findById(productReq.getProductTypeId())
                .orElseThrow(DataNotFoundException::productTypeNotFound);

        Unit unit = unitRepo.findById(productReq.getUnitId())
                .orElseThrow(DataNotFoundException::unitNotFound);

        var isExist = productRepo.findByProductCode(productReq.getCode());
        if (isExist.isPresent())
            throw AlreadyExistException.productCodeAlreadyExist();

        Product product = productStructMapper.mapToEntitySave(productReq, productType, unit);

        return productStructMapper.mapToDto(productRepo.save(product));
    }

    @Override
    public ProductDto update(Long id, ProductReq productReq) {
        var product = productRepo.findById(id)
                .orElseThrow(DataNotFoundException::productNotFound);

        var productType = productTypeRepo.findById(productReq.getProductTypeId())
                .orElseThrow(DataNotFoundException::productTypeNotFound);

        var unit = unitRepo.findById(productReq.getUnitId())
                .orElseThrow(DataNotFoundException::unitNotFound);

        var isExist = productRepo.existsByProductCodeAndIdNot(productReq.getCode(), id);
        if (isExist)
            throw AlreadyExistException.productCodeAlreadyExist();

        productStructMapper.mapToEntity(product, productReq, productType, unit);
        product = productRepo.save(product);

        return productStructMapper.mapToDto(product);
    }

}
