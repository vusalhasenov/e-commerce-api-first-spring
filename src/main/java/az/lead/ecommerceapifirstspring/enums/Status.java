package az.lead.ecommerceapifirstspring.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum Status {
    ENABLED(1),
    DISABLED(0);

    private static final Map<Integer, Status> VALUE_MAP = new HashMap<>();

    static {
        for (Status type : values()) {
            VALUE_MAP.put(type.value, type);
        }
    }

    private final int value;


    public static Status getStatus(Integer value) {
        return VALUE_MAP.get(value);
    }

    public static Optional<Status> getStatusEnum(Integer value) {
        return Optional.ofNullable(VALUE_MAP.get(value));
    }


}
