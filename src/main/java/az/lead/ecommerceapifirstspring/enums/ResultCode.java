package az.lead.ecommerceapifirstspring.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {

    PRODUCT_ALREADY_EXIST(-1,"product name already exist"),
    PRODUCT_CODE_ALREADY_EXIST(-1,"product code already exist"),

    PRODUCT_NOT_FOUND(-2,"product not found"),
    URL_NOT_FOUND(-2,"url not found"),
    PRODUCT_TYPE_NOT_FOUND (-2,"product type not found"),
    UNIT_NOT_FOUND(-2,"unit type not found"),
    USER_NOT_FOUND(-2,"invalid credentials"),

    INVALID_CREDENTIALS(-5,"invalid credentials"),
    INVALID_FIELD_VALUE(-3,"invalid field value"),
    REQUEST_METHOD_NOT_SUPPORTED(-9,"request method not supported" ),

    SYSTEM_ERROR(-10,"system error"),
    OK(1,"success");



    private int code;
    private String value;


}
