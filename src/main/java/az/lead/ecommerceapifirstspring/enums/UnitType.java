package az.lead.ecommerceapifirstspring.enums;

import java.util.HashMap;
import java.util.Map;

public enum UnitType {
    KQ(1L),
    LITR(2L),
    EDED(3L);

    private final Long value;

    public static final Map<Long,UnitType> VALUE_MAP= new HashMap<>();
    static {
        for (UnitType status:values()) {
            VALUE_MAP.put(status.value,status);
        }
    }

    private UnitType(Long value) {
        this.value = value;
    }
    public Long getValue() {
        return value;
    }

    public static UnitType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}
