package az.lead.ecommerceapifirstspring.mapper;

import az.lead.ecommerceapifirstspring.dto.ProductDto;
import az.lead.ecommerceapifirstspring.entity.Product;
import az.lead.ecommerceapifirstspring.entity.ProductType;
import az.lead.ecommerceapifirstspring.entity.Unit;
import az.lead.ecommerceapifirstspring.request.ProductReq;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductStructMapper {


    @Mapping(target = "product.productCode",source = "request.code")
    @Mapping(target = "product.productTypeId",source = "type")
    @Mapping(target = "product.unitId",source = "unit")
    @Mapping(target = "product.name",source = "request.name")
    @Mapping(target = "id",ignore = true)
    void mapToEntity(@MappingTarget Product product,
                        ProductReq request,
                        ProductType type,
                        Unit unit);

    @Mapping(target = "productTypeId",source = "productTypeId.name")
    @Mapping(target = "unitId",source = "unitId.name")
    ProductDto mapToDto(Product product);

    @Mapping(target = "productCode",source = "request.code")
    @Mapping(target = "productTypeId",source = "type")
    @Mapping(target = "unitId",source = "unit")
    @Mapping(target = "name",source = "request.name")
    @Mapping(target = "id",ignore = true)
    Product mapToEntitySave(ProductReq request,
                     ProductType type,
                     Unit unit);
}
